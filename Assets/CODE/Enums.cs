﻿namespace UnityCommunityProject
{
    public enum ColliderType
    {
        Undefined = -1,
        OuterZone = 0,
        InnerZone = 1
    }

    public enum MessageType
    {
        Undefined = -1,
        NearStructure = 0,
        NoLongerNearStructure = 1,
        EnteredBuilding = 2,
        ExitedBuilding = 3
    }

    public enum ObjectType
    {
        Undefined = -1,
        Building = 0,
        ResourceStructure = 1
    }

    public enum ElementalDamageType
    {
        Neutral = 0,
        Fire = 1
    }
}