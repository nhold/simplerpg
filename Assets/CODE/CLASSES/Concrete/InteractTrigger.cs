﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractTrigger : MonoBehaviour {

	[SerializeField]
	private GameObject interactableObj;
	private IInteract interactable;

	void Awake() {
		interactable = interactableObj.GetComponent<IInteract>();
	}

	void OnTriggerEnter(Collider other) {
		if(other.CompareTag("Player")) {
			if(interactable != null) {
				interactable.Interact();
			}
		}
	}

	void OnTriggerExit(Collider other) {
		if(other.CompareTag("Player")) {
			if(interactable != null) {
				interactable.EndInteract();
			}
		}
	}

}
